# WK180 ROPTool target

Use this in conjunction with roptool to generate ROP files.
You can then use those ROP files with HTMLIt to create a runnable payload

[http://lolhax.org](http://lolhax.org)

[Follow me on Twitter @DaveeFTW](https://twitter.com/DaveeFTW)